from django.shortcuts import render, redirect
from accounts.forms import LoginForm, SignUpForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User


# Create your views here.
def login_form(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect('home')
    else:  # else statemnt lines 22-26 is the GET method
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, 'accounts/login.html', context)


def user_logout(request):
    logout(request)
    return redirect("login")  # specified in feature 9 step 1


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']
            if password == password_confirmation:
                user = User.objects.create_user(  # create_user is a method in django
                    username,
                    password=password,
                )
                login(request, user)  #login is a function in django
                return redirect("home")
            else:
                form.add_error("password", "Passwords do no match")
    else:
        form = SignUpForm()
    context = {
        "form": form,
    }
    return render(request, "registration/signup.html", context)
