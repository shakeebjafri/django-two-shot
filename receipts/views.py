from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# line 3 is required for filtering what to see when a user is logged in


# Create your views here.
@login_required  # added in feature 8 Filtering Receipt
def show_receipt(
    request,
):  # function is being called from Receipt in models.py
    receipts = Receipt.objects.filter(
        purchaser=request.user
    )  # Receipt for models.py
    context = {"receipts": receipts}
    return render(request, "receipts/receipt_list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {"form": form}
    return render(request, "receipts/create.html", context)


# Feature 12 List Views


@login_required
def category_list(request):
    expensecategory = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "expensecategorys": expensecategory  # expensecategorys is used in line 20 for the loop function in category_list.html
    }
    return render(request, "receipts/category_list.html", context)


@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        "accounts": account  # accounts is being used in account_list.html
    }
    return render(request, "receipts/account_list.html", context)


# Feature 13 Create Category Views


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            categories = form.save(False)
            categories.owner = request.user
            categories.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {"form": form}
    return render(request, "receipts/create_category.html", context)


# Feature 14 Create Account Views
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            accounts = form.save(False)
            accounts.owner = request.user
            accounts.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "receipts/create_account.html", context)
